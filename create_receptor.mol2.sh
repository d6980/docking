#!/bin/bash

# This script is for generating a mol2 with sybyl atom types and partial charges
# It reads in a pdb and creates a parm7/coord file via tleap and then uses
# ambpdb to create the mol2 with sybyl atoms types using a lookup table, which
# is the file $AMBERHOME/dat/antechamber/ATOMTYPE_CHECK.TAB. If your receptor has
#atomtypes that are not in this file they will need to be manually add them.



if [ "$#" -ne 2 ]; then
    echo "Usage:	$0 {input receptorpdb} {output receptormol2}"
  
    echo "	
	This script assumes that the protein has been properly prepared, correct protonation,
	no missing residues, etc. If your receptor has non-standard residues, simply
	edit the portion of the script that creates the tleap input and 
	add the necessary tleap commands"
    exit
fi

receptorpdb=$1
receptormol2=$2


#Load modules to use AMBER
module load amber/latest

if [ -z $AMBERHOME ]; then
  echo "Error: AMBERHOME is not set."
  echo "Please set AMBERHOME in your environment"
fi

echo 

if [ ! -f ${receptorpdb} ]; then
  echo "Error: The receptor file ${receptorpdb} does not exist"
  exit
fi

echo "..Running tLeAp .."
cat >__tleap.in <<EOF
source leaprc.protein.ff14SB
source leaprc.water.tip3p

REC =loadpdb ${receptorpdb}

saveamberparm REC __rec.parm7 __rec.rst7
quit
EOF

$AMBERHOME/bin/tleap -f __tleap.in

sleep 2

if [ ! -s __rec.parm7 ] || [ ! -s __rec.rst7 ]; then
  echo "Error: The topology and coordinate files were not properly generated"
  echo "       Please look for error in the leap.log file"
else
   echo "  "
   echo "..Creating the mol2 file"
   echo " "
   ${AMBERHOME}/bin/ambpdb -p __rec.parm7 -c __rec.rst7 -mol2 -sybyl > $receptormol2
   rm -f __rec.parm7 __rec.rst7 __tleap.in
fi

exit

