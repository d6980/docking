#!/usr/bin/env python

import os,sys,shutil
import rdkit
from rdkit import Chem
from rdkit.Chem import AllChem

def extractlig(inputfile,OverWrite=False):
   if os.path.exists(inputfile):
      extension = os.path.splitext(inputfile)[1]
      if extension == ".mol2":
          xfs = Chem.MolFromMol2File(inputfile)
      elif extension == ".sdf" or extension == ".sd":
          xfs = [m for m in Chem.ForwardSDMolSupplier(
                  inputfile, removeHs=False) if m is not None]
      elif extension == ".pdb":
           xfs = Chem.MolFromPDBFile(inputfile)

   namemol2= ""

   ifs=""
   if os.path.exists("ligands") and not OverWrite:
       sys.exit("The directory 'ligands/' already exists, pleases specify the -O option or manually remove it")
   elif os.path.exists("ligands") and OverWrite:
       shutil.rmtree("ligands")
       os.mkdir("ligands")
   else:
       os.mkdir("ligands")
   for mol in xfs:
       namemol= mol.GetProp("_Name")
       #print(namemol,namemol2)
       if namemol != namemol2:
           #Chem.MolToMolFile(mol, "%s.sdf" % namemol)
           try:
               ifs.close()
           except AttributeError:
               pass
           ifs=Chem.SDWriter("%s.sdf" % namemol)
           ifs.write(mol)
           if os.path.isfile("%s.sdf" % namemol2):
               os.mkdir("ligands"+"/"+namemol2)
               shutil.copy("%s.sdf" % namemol2, "ligands"+"/"+namemol2+"/ligand.sdf")
               shutil.move("%s.sdf" % namemol2, "ligands"+"/"+namemol2+"/%s.sdf" % namemol2)
           namemol2=namemol
       else:
           #Chem.MolToMolFile(mol, "%s.sdf" % namemol)
           ifs.write(mol)

   #Copy the last conformer to the ligands directory
   os.mkdir("ligands"+"/"+namemol)
   ifs.close()
   shutil.copy("%s.sdf" % namemol2, "ligands"+"/"+namemol2+"/ligand.sdf")
   shutil.move("%s.sdf" % namemol, "ligands"+"/"+namemol+"/%s.sdf" % namemol)




extractlig(sys.argv[1],True)

