#/bin/bash/

export RBT_ROOT=/software/rDock_2013.1_src
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$RBT_ROOT/lib
export PATH=$PATH:$RBT_ROOT/bin
sdf=$1
sdsplit -10000 -ochunk_ ${sdf}
