#!/bin/bash 
#SBATCH -p project
#SBATCH -n 2
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --qos=maxjobs
#SBATCH --oversubscribe
#SBATCH --time=01:00:00
#SBATCH -A d2bvant
#SBATCH --reservation=d2bvant

if [ "$#" -ne 5 ]; then
   printf "\n\tUSAGE: $0 <receptor-pdb>  <receptor-mol2>  <reference-sdf> <grid file>   <dir-list> \n"
   exit 1
fi

receptorpdb=`realpath $1`
receptormol2=`realpath $2`
reference=`realpath $3`
gridfile=`realpath $4`

source /software_common/anaconda3/bin/activate apps
module load jchemsuite/20.18

readarray dockdir < $5

dockval=${dockdir[SLURM_ARRAY_TASK_ID]}
dockname=`basename ${dockval}`
ligand=`basename ${dockval}`

node=`hostname`
user=`whoami`


#ncpus=2
cd ${dockval}
workdir=`pwd`


date
printf "===================================================================\n"
printf "\tRunning on compute node: ${node}\n"
printf "\tDocking Compound: ${dockname}\n"
printf "===================================================================\n"
printf "\n\n"

cleanup_dir () {
printf "\nError: $1\n"
cd $2
rm -f ligand.sdf
rm -rf /dev/shm/$3/$4
}




rm -f ligand.sdf

echo ${reference}
if [ ! -f ${dockname}.sdf ]; then

   echo "Missing the file ${dockname}.sdf"
fi
         pwd
         #Run the docking via smina (If the directory has *conformers.sdf then use that) and take the best 3 ranked conformers
         time /projects2/insite/dwight/KRAS/2021.10.19.FragRxnEnum/conformers-nopKA.py  -confnum 1 -confenergy 5 \
              -rms 1.5 -o ${dockname}.conformers.sdf -l ${dockname}.sdf
         rm -f unsuccessful_generated.conformers.sdf
         if [ ! -f ${dockname}.conformers.sdf ]; then
             cleanup_dir "Conformer generation not successful" "${workdir}" "${user}" "${dockname}"
             exit 1
          fi

sleep 2
       cp  /projects2/insite/dwight/KRAS/2021.10.19.FragRxnEnum/_rdock_grid/receptor.* .
        /software_common/RBFE_pose-placement_latest/mcss_align.py -r ${reference} -l ${dockname}.conformers.sdf -o  ${dockname}.aligned.sdf -rdkitmcs
        sleep 4

 if [ ! -s ${dockname}.aligned.sdf ]; then

    echo "MCS Alignment Failed:"

  fi

cp $gridfile .       
/software_common/RBFE_pose-placement_latest/rdock_run.py -allH -n 10 -rec ${receptormol2}  -grid receptor.as -r ${reference} -do ${ligand}.rdock -l ${dockname}.aligned.sdf


if [ ! -s ligand.sdf ]; then
     cleanup_dir "The Docking calculation not successful" "${workdir}" "${user}" "${dockname}"
     exit 1
fi

#mv ligand.sdf ligand_noformalcharge.sdf
#obabel -isdf ligand_noformalcharge.sdf --p 7.4 -O ligand.sdf

vinardo=`/software/smina/bin/smina.static --autobox_ligand ${reference} -l ligand.sdf -o t.sdf --score_only -r ${receptorpdb}  --log x.log --scoring vinardo   | grep Affinity | awk '{print $2}'`
smina=`/software/smina/bin/smina.static --autobox_ligand ${reference} -l ligand.sdf -o x.sdf --score_only -r ${receptorpdb} --log y.log    | grep Affinity | awk '{print $2}'`

echo "Ligand,vinardo,smina" > ${ligand}.results.csv
echo ${ligand},${vinardo},${smina} >> ${ligand}.results.csv



rm -rf /dev/shm/${user}/${dockname}
rm -rf  t.sdf x.sdf x.log y.log *.prm *.as *.conformers.shape-align.sdf *.conformers.sdf *chxm.sdf *.sd
exit 0
##############################################################

