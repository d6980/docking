#!/bin/bash
#SBATCH -o htvs.job.output.dat
#SBATCH -p project
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --qos=maxjobs
#SBATCH --oversubscribe
#SBATCH -J docking_job-%J
#SBATCH --reservation=d2bvant
#SBATCH -A d2bvant

date

printf "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
printf "\n\t Running HTVS SiTX Therapeutics Workflow            \n"
printf "\n\t  `date`     \n"
printf "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"


user=`whoami`

maxjobs=1000

mkdir -p logs

for i in {1..5}; do
               mkdir -p logs/chunk_${i}


                total_jobs=`squeue -u ${user} -r -h | wc -l`
                while [ ${total_jobs} -gt ${maxjobs} ]; do
                    sleep 10
                    #echo "sleep for 2 minutes"
                    total_jobs=`squeue -u ${user} -r -h | wc -l`

               done
               echo "Submitting jobs for chunk_${i}"
               num=`wc -l chunk_${i}.list.dat | awk '{print $1}'`
               echo ${num}
               numdockdir=$((${num}-1))
sbatch -a 0-${numdockdir}%2000   -o `realpath logs/chunk_${i}/job%A.%a.log` dock.run.sh protein_docking.pdb  \
                                  protein.mol2 ref_ligand_amide.sdf receptor.as chunk_${i}.list.dat 
date

done
